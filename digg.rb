require 'rest-client'
require 'json'
require "colorize"

class Digg_news
  puts "-Presiona (3) para Digg!!".yellow
  def initialize
    @base_url =  'http://digg.com/api/news/popular.json'
  end

  def get_response
    @response = RestClient.get( @base_url )
  end

  def parse_json
    @json = JSON.parse( @response.body )
  end
  
  def digg_feed
   
    @json['data']['feed'].each do |data|
    time = Time.at("#{data['date']}".to_i)
    puts "   Titulo:  #{ data['content']['title'] }".green
    puts "   Autor:   #{ data['content']['author'] }".cyan
    puts "   Fecha:  #{ time.strftime("%d/%m/%Y")}".blue
    puts "   Visita aqui:  #{ data['content']['url'] }\n\n\n-------------------------------------------------------------------------------------------------------------------------------------\n\n".light_blue
    end
  end

  def create_feed
    get_response
    parse_json
    digg_feed
  end
end

