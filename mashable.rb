require 'rest-client'
require 'json'
require "colorize"

class Mashable_news
  puts "-Presiona (2) para Mashable!!".yellow
  def initialize
    @base_url = 'https://mashable.com/stories.json'
  end

  def get_response
    @response = RestClient.get( @base_url )
  end

  def parse_json
    @json = JSON.parse( @response.body )
  end

  def mashable_feed
    
    puts "\n New: \n\n"

    @json['new'].each do |data|
      date = Date.rfc2822( data['post_date_rfc'] )
      puts "   Titulo:  #{ data['title'] }".green
      puts "   Autor:   #{ data['author'] }".cyan
      puts "   Fecha:  #{ date.mday }/#{ date.mon }/#{ date.year }".blue
      puts "   Visita aqui:   #{ data['link'] }\n\n\n-------------------------------------------------------------------------------------------------------------------------------------\n\n".light_blue
    end

    puts "\n Rising: \n\n"

    @json['rising'].each do |data|
      date = Date.rfc2822( data['post_date_rfc'] )
      puts "   Titulo:  #{ data['title'] }".green
      puts "   Autor:   #{ data['author'] }".cyan
      puts "   Fecha:  #{ date.mday }/#{ date.mon }/#{ date.year }".blue
      puts "   Visita aqui:   #{ data['link'] }\n\n\n-------------------------------------------------------------------------------------------------------------------------------------\n\n".light_blue
    end
     
    puts "\n Hot: \n\n"

    @json['hot'].each do |data|
      date = Date.rfc2822( data['post_date_rfc'] )
      puts "   Titulo:  #{ data['title'] }".green
      puts "   Autor:   #{ data['author'] }".cyan
      puts "   Fecha:  #{ date.mday }/#{ date.mon }/#{ date.year }".blue
      puts "   Visita aqui:   #{ data['link'] }\n\n\n-------------------------------------------------------------------------------------------------------------------------------------\n\n".light_blue
    end

  end

  def create_feed
    get_response
    parse_json
    mashable_feed
  end
end







