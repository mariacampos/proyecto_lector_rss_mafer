require 'rest-client'
require 'json'
require 'colorize'

class Reddit_news
    
    print "INGRESE SU NOMBRE:".cyan
    name = gets.chomp.to_s
    puts "Hola #{name}, ¿Que deseas leer hoy?".cyan
    puts "-Presiona (1) para Reddit!!".yellow

  def initialize
    @base_url = 'https://www.reddit.com/.json' 
  end

  def get_response
    @response = RestClient.get( @base_url )
  end

  def parse_json
    @json = JSON.parse( @response.body )
  end
  
  def reddit_feed
   
    @json['data']['children'].each do |data|
    time = Time.at("#{data['data']['created_utc']}".to_i)
    puts "   Titulo:  #{ data['data']['title'] }".green
    puts "   Autor:   #{ data['data']['author'] }".cyan
    puts "   Fecha:  #{ time.strftime("%d/%m/%Y")}".blue
    puts "   Visita aqui:  #{ data['data']['url'] }\n\n\n-------------------------------------------------------------------------------------------------------------------------------------\n\n".light_blue
    end
  end

  def create_feed
    get_response
    parse_json
    reddit_feed
  end
end

